# bnexttest

## Description

Prueba Técnica- [Backend Developer] Desarrollo de Producto

## Installation

```bash
$ yarn install
```
## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test
```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e
```

## Documentation
```bash
# local
localhost:3000/api

# deployed
https://bnexttest.herokuapp.com/api

```

## Stay in touch

- Author - [Rubén O. Alvarado](https://www.linkedin.com/in/ruben-alvarado-molina-9020010/)
- Github - [RubenOAlvarado](https://github.com/RubenOAlvarado)
- Gitlab - [Munditoro](https://gitlab.com/Munditoro)
