import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, HttpStatus } from '@nestjs/common';
import * as request from 'supertest';

import { AppModule } from '../src/app.module';
import * as mongoose from 'mongoose';
import { NewUserDTO } from 'src/users/model/dtos/newuser.dto';
import { AddContactsDTO } from 'src/users/model/dtos/addcontacts.dto';
import { NewContactDTO } from 'src/users/model/dtos/newcontact.dto';
import { CommonDTO } from 'src/users/model/dtos/common.dto';

describe('E2E Test for BnextTest Endpoints', () => {
    let app: INestApplication;

    beforeEach(async () => {
        jest.setTimeout(10000);
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule]
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('Should create a user', () => {
        const newUser: NewUserDTO = {
            name: 'Pepe',
            lastName: 'Perez',
            phone: '+525517281819'
        }

        return request(app.getHttpServer())
                .post('/api/V1/users/create')
                .set('Accept', 'application/json')
                .send(newUser)
                .expect(HttpStatus.CREATED);
    });

    it('Should get an user by his id', () => {
        const id = '5fea9fec8a75452ad725cecb';

        return request(app.getHttpServer())
                .get(`/api/V1/users/${id}`)
                .set('Accept', 'application/json')
                .expect(HttpStatus.OK);
    });

    it('Should contacts to a user account', () => {
        const newContact: NewContactDTO = {
            contactName: 'Luis',
            phone: '+525566778823'
        };
        const addContacts: AddContactsDTO = {
            user: '5fea9fec8a75452ad725cecb',
            contacts: [newContact]
        };
        
        return request(app.getHttpServer())
                .post('/api/V1/users/addContacts')
                .set('Accept', 'application/json')
                .send(addContacts)
                .expect(HttpStatus.OK);
    });

    it('Should return a list with the contacts in common from two users', () => {
        const usersToCompare: CommonDTO = {
            firstUser: '5fea9fec8a75452ad725cecb',
            secondUser: ''
        };

        return request(app.getHttpServer())
                .post('/api/V1/users/compare')
                .set('Accept', 'application/json')
                .send(usersToCompare)
                .expect(HttpStatus.OK);
    });

    afterAll(async done => await mongoose.disconnect(done));
});