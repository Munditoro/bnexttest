import { Test, TestingModule } from '@nestjs/testing';
import { PhoneValidator } from './model/phone.validator.interface';
import { PhoneService } from './phone.service';

describe('PhoneService', () => {
  let service: PhoneService;

  const dummyPhone = '+525515681938';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PhoneService],
    }).compile();

    service = module.get<PhoneService>(PhoneService);
  });

  it('should validate a phone number', async () => {
        const data:PhoneValidator = {
          valid: true,
          type: 'mobile',
          international_calling_code: 52,
          international_number: '+525515681938',
          local_number: '5515681938',
          location: 'Mx',
          country: 'Mexico',
          country_code: 'MX',
          country_code3: 'MX',
          currency_code: 'Mx',
          is_mobile: false,
          prefix_network: '52'
        };

        jest.spyOn(service, 'validatePhone').mockImplementation(async () => await data);

        const {valid} = await service.validatePhone(dummyPhone);
        expect(valid).toBe(true);
    });
});
