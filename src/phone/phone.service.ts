import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import { PhoneValidator } from './model/phone.validator.interface';
import axios from 'axios';
import FormData = require("../../node_modules/form-data");
import 'dotenv/config';

@Injectable()
export class PhoneService {

    private readonly logger = new Logger(PhoneService.name);

    async validatePhone(phone:string):Promise<PhoneValidator>{
        try {

            this.logger.debug(`Validating phone ${phone} in neutrino API`);

            const url = `${process.env.NEUTRINO_URL}/phone-validate`;

            const form = new FormData();
            form.append('user-id', process.env.USER_ID);
            form.append('api-key', process.env.API_KEY);
            form.append('number', phone);

            const response = await axios.post(url, form, { headers: form.getHeaders() });

            return response.data;

        } catch (error) {
            this.logger.error(`Error validating phone number: ${error}`);
            throw new InternalServerErrorException(`Error validating phone number: ${error.message}`);
        }
    }
}