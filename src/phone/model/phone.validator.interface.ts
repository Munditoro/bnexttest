export interface PhoneValidator{
    readonly valid: boolean;
    readonly type: string;
    readonly international_calling_code: number;
    readonly international_number: string;
    readonly local_number: string;
    readonly location: string;
    readonly country: string;
    readonly country_code: string;
    readonly country_code3: string;
    readonly currency_code: string;
    readonly is_mobile: boolean;
    readonly prefix_network: string;
}