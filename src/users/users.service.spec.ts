import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersSchema } from './model/users.schema';
import { UsersService } from './users.service';
import { PhoneService } from '../phone/phone.service';
import { closeInMongodConnection, rootMongooseTestModule } from '../../test/utils/MongooseTestModule';
import { NewUserDTO } from './model/dtos/newuser.dto';
import { UrlValidator } from './model/dtos/ulr.validator.dto';
import { BadRequestException, NotFoundException} from '@nestjs/common';
import { AddContactsDTO } from './model/dtos/addcontacts.dto';
import { NewContactDTO } from './model/dtos/newcontact.dto';
import { PhoneModule } from '../phone/phone.module';
import { PhoneValidator } from 'src/phone/model/phone.validator.interface';
import { CommonDTO } from './model/dtos/common.dto';
import { User } from './model/users.interface';

describe('UsersService', () => {
  let service: UsersService;
  let phoneService: PhoneService;
  
  const dummyUser: NewUserDTO = {
    name: 'Pepe',
    lastName: 'Perez',
    phone: '+525587198191'
  };

  const badPhoneUser: NewUserDTO = {
    name: 'Pepe',
    lastName:'Luis',
    phone: '12345678'
  };

  const phoneValidator:PhoneValidator = {
    valid: true,
    type: 'mobile',
    international_calling_code: 52,
    international_number: '+525515681938',
    local_number: '5515681938',
    location: 'Mx',
    country: 'Mexico',
    country_code: 'MX',
    country_code3: 'MX',
    currency_code: 'Mx',
    is_mobile: false,
    prefix_network: '52'
  };

  const phoneValidatorError:PhoneValidator = {
    valid: false,
    type: 'mobile',
    international_calling_code: 52,
    international_number: '+525515681938',
    local_number: '5515681938',
    location: 'Mx',
    country: 'Mexico',
    country_code: 'MX',
    country_code3: 'MX',
    currency_code: 'Mx',
    is_mobile: false,
    prefix_network: '52'
  };

  const common:CommonDTO = {
    firstUser: '5fec53fafe7f64bf7189326c',
    secondUser: '5fec5407fe7f64bf7189326d'
  };

  const registeredContact:User = {
    _id:"5fec557afe7f64bf7189326e",
    name:"avt",
    lastName:"Perez",
    phone:"+34666666661"
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:[
        rootMongooseTestModule(),
        PhoneModule,
        MongooseModule.forFeature([{name:'Users', schema: UsersSchema}])
      ],
      providers: [UsersService],
    }).compile();

    service = module.get<UsersService>(UsersService);
    phoneService = module.get<PhoneService>(PhoneService);
  });

  it('can be created correctly', async () => {

      jest.spyOn(phoneService, 'validatePhone').mockImplementation(async () => await phoneValidator);

      const newUSer = await service.createUser(dummyUser);
      
    expect(newUSer._id).toBeDefined();
      expect(newUSer.name).toBe(dummyUser.name);
      expect(newUSer.lastName).toBe(dummyUser.lastName);
      expect(newUSer.phone).toBe(dummyUser.phone);
  });

it('The phone number must be valid', async () => {

      jest.spyOn(phoneService, 'validatePhone').mockImplementation(async () => await phoneValidatorError);

      await expect(service.createUser(badPhoneUser))
            .rejects
            .toThrow(BadRequestException);
  });

it('should retrieve the contacts list from a user', async () => {

      jest.spyOn(phoneService, 'validatePhone').mockImplementation(async () => await phoneValidator);

      const currentUser = await service.createUser(dummyUser);

      const dummyGuy:NewContactDTO = {
        contactName: 'Pepe',
        phone: '+525578912390'
      };

      const dummyContact: AddContactsDTO = {
        user: currentUser._id,
        contacts: [dummyGuy] 
      };

      await service.addContacts(dummyContact);

      const id:UrlValidator = {id:currentUser._id};

      await expect(service.getUserContacts(id)).not.toBeNull();
  });

  it('should retrieve an NOTFOUNDEXCEPTION because the user does not exist', async () => {
        jest.spyOn(service, 'getUser').mockImplementation(async () => await null);
        
        const id:UrlValidator = {id:'5fea9fec8a75452ad725abcd'};

        await expect(service.getUserContacts(id))
              .rejects
              .toThrow(NotFoundException);
    });

it('add contacts to an user correctly', async () => {

      jest.spyOn(phoneService, 'validatePhone').mockImplementation(async () => await phoneValidator);

      const currentUser = await service.createUser(dummyUser);

      const dummyGuy:NewContactDTO = {
        contactName: 'Pepe',
        phone: '+525578912390'
      };

      const dummyContact: AddContactsDTO = {
        user: currentUser._id,
        contacts: [dummyGuy] 
      };

      expect(async () => await service.addContacts(dummyContact))
        .not
        .toThrow();
  });

  it('should retrieve contacts in common that are registered in the db from two users', async () => {
      jest.spyOn(service, 'validateUsersToCompare').mockImplementation(async () => await true);
      jest.spyOn(service, 'getRegisteredContacts').mockImplementation(async () => await [registeredContact]);

      const usersInCommon:User[] = await service.compareContacts(common);
    
      expect(usersInCommon[0].phone).toContain('+34666666661');

  });

  afterAll(async () => {
    await closeInMongodConnection();
  });
});
