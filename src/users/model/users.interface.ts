import { Document } from 'mongoose';

export interface User extends Document{
    readonly _id: string;
    readonly name: string;
    readonly lastName?: string;
    readonly phone: string;
    readonly contacts?: any;
}