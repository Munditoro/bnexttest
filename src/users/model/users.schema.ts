import * as mongoose from 'mongoose';
const {Schema} = mongoose;

export const UsersSchema = new Schema({
    name:{
        type:String,
        required: true,
        index: true
    },
    lastName: {
        type:String,
        required: true
    },
    phone:{
        type: String,
        required: true,
        index: true
    },
    contacts: Schema.Types.Mixed
});