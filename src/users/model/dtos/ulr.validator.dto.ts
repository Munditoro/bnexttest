import { ApiProperty } from "@nestjs/swagger";
import { IsMongoId, IsNotEmpty, IsString } from "class-validator";

export class UrlValidator{
    @ApiProperty({
        description: 'Id from the user to look',
        type: String
    })
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    id: string;
}