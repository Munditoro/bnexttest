import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsPhoneNumber, IsString, MaxLength, MinLength } from "class-validator";

export class NewUserDTO{
    @ApiProperty({
        description: 'User name',
        example: 'Pepe',
        type: String
    })
    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(150)
    name: string;

    @ApiPropertyOptional({
        description: 'User lastName',
        example: 'Perez',
        type: String
    })
    @IsOptional()
    @IsString()
    @MinLength(3)
    @MaxLength(150)
    lastName?: string;

    @ApiProperty({
        description: 'User phone number; must be a valid phone number',
        example: '+5257181912',
        type: String
    })
    @IsNotEmpty()
    @IsString()
    @IsPhoneNumber(null)
    phone: string
}