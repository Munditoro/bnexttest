import { NewContactDTO } from "./newcontact.dto";

export class AddContactsResponse{
    savedContacts?: Array<NewContactDTO>;
    invalidContacts?: any;
}