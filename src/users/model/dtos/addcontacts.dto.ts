import { ApiExtraModels, ApiProperty } from "@nestjs/swagger";
import { ArrayNotEmpty, IsMongoId, IsNotEmpty, ValidateNested } from "class-validator";
import { NewContactDTO } from "./newcontact.dto";

@ApiExtraModels(NewContactDTO)
export class AddContactsDTO{
    @ApiProperty({
        description: 'Id from the user to update; must be a mongoId',
        type: String
    })
    @IsNotEmpty()
    @IsMongoId()
    user: string;

    @ApiProperty({
        description: 'Array with the contacts that the user wants to save',
        type: [NewContactDTO]
    })
    @ArrayNotEmpty()
    @ValidateNested({each: true})
    contacts: Array<NewContactDTO>
}