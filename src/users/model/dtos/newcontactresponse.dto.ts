export class NewContactResponse{
    message?: string;
    contactName: string;
    phone: string;
}