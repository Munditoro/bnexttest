import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsPhoneNumber, IsString, MaxLength, MinLength } from "class-validator";

export class NewContactDTO{
    @ApiProperty({
        description: 'Contact name',
        type: String
    })
    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    @MaxLength(150)
    contactName: string;

    @ApiProperty({
        description: 'Contact phone number; must be a valid phone number',
        example: '+5257181912',
        type: String
    })
    @IsNotEmpty()
    @IsString()
    @IsPhoneNumber(null)
    phone: string;
}