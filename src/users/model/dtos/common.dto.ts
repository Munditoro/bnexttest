import { ApiProperty } from "@nestjs/swagger";
import { IsMongoId, IsNotEmpty } from "class-validator";

export class CommonDTO{
    @ApiProperty({
        description: 'First user to check; it must be an mongoId',
        type: String
    })
    @IsNotEmpty()
    @IsMongoId()
    firstUser: string;

    @ApiProperty({
        description: 'The second user to check; it must be an mongoId',
        type: String
    })
    @IsNotEmpty()
    @IsMongoId()
    secondUser: string;
}