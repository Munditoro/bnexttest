import { BadRequestException, Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PhoneService } from '../phone/phone.service';
import { AddContactsDTO } from './model/dtos/addcontacts.dto';
import { AddContactsResponse } from './model/dtos/addcontactsresponse.dto';
import { CommonDTO } from './model/dtos/common.dto';
import { NewContactDTO } from './model/dtos/newcontact.dto';
import { NewUserDTO } from './model/dtos/newuser.dto';
import { UrlValidator } from './model/dtos/ulr.validator.dto';
import { User } from './model/users.interface';

@Injectable()
export class UsersService {
    constructor(
        @InjectModel('Users') private readonly usersModel: Model<User>,
        private phoneValidatorService: PhoneService
    ){}

    private readonly logger = new Logger(UsersService.name);

    async createUser(newUserDTO: NewUserDTO):Promise<User>{
        try {
            this.logger.debug('Adding new user');
            this.logger.debug('Validating phone number');
            const {phone} = newUserDTO;

            //todo validar que el teléfono no esté registrado en la bd
            const user = await this.getUserByPhone(phone);

            if(!user){
                const {valid} = await this.phoneValidatorService.validatePhone(phone);
            
                if(valid){
                    this.logger.debug('Valid phone');
                    const newUser = new this.usersModel(newUserDTO);
                    return newUser.save();
                }else{
                    this.logger.error('Phone invalid');
                    throw new BadRequestException('Send a valid phone number');
                }
            }else{
                this.logger.debug('Phone number already registered');
                throw new BadRequestException(`An user is already registered in the database with the phone: ${phone} `);
            }

        } catch (error) {
            this.logger.error(`Error creating user: ${error}`);
            if(error instanceof BadRequestException)
                throw error;
            else
                throw new InternalServerErrorException(`Error creating user: ${error.message}`);
        }
    }

    async getUserContacts(params: UrlValidator):Promise<[]>{
        try {
            //todo validar que el usuario exista
            const {id} = params;

            const user = await this.getUser(id);
            
            if(!user) throw new NotFoundException('User does not exist in database');
            
            return user.contacts;
        } catch (error) {
            this.logger.error(`Error looking user: ${error}`);
            if(error instanceof NotFoundException)
                throw error;
            else
                throw new InternalServerErrorException(`Error looking user: ${error.message}`);
        }
    }

    //contacts methods
    async compareContacts(commonDTO:CommonDTO):Promise<Array<User>>{
        try {
            //todo validar los contactos que sean usuarios de bnext
            this.logger.debug('Initializing contacts compare');
            const {firstUser, secondUser} = commonDTO;
            this.logger.debug('Getting users data');
            const user1 = await this.getUser(firstUser);
            const user2 = await this.getUser(secondUser);

            this.logger.debug('Validating users');

            const valid = await this.validateUsersToCompare(user1, user2);
            
            if(valid){
                this.logger.debug('Comparing contacts from each user');
                
                const registeredContactsUser1 = await this.getRegisteredContacts(user1);
                const registeredContactsUser2 = await this.getRegisteredContacts(user2);

                if(!registeredContactsUser1 && !registeredContactsUser2)
                    throw new BadRequestException('Users does not have contacts registered as users');
                else if(!registeredContactsUser1)
                    throw new BadRequestException(`User ${firstUser} does not have contacts registered as users`);
                else if(!registeredContactsUser2)
                throw new BadRequestException(`User ${secondUser} does not have contacts registered as users`);


                return registeredContactsUser1.filter(({phone: phone1}) => registeredContactsUser2.some(({phone: phone2}) => phone1 === phone2));
            }

        } catch (error) {
            this.logger.error(`Error comparing contacts: ${error}`);
            if(error instanceof NotFoundException || error instanceof BadRequestException)
                throw error;
            else
                throw new InternalServerErrorException(`Error comparing contacts: ${error.message}`);
        }
    }

    async addContacts(addContactDTO: AddContactsDTO):Promise<AddContactsResponse>{
        try {
            this.logger.debug('Adding contacts to user');
            const {user, contacts} = addContactDTO;
            this.logger.debug('Validating contacts phone numbers');
            const validUser = await this.getUser(user);

            if(validUser){
                this.logger.debug('Its a valid user');

                const validContacts:AddContactsResponse = await this.validateContacts(contacts, validUser.contacts);

                this.logger.debug('Contacts validated');

                if(validContacts.savedContacts){
                    await this.usersModel.findByIdAndUpdate(user, {contacts: validContacts.savedContacts});
                    this.logger.debug('Valid contacts added');
                }

                return validContacts;
            }else{
                this.logger.error(`The user with id: ${user} does not exist in the db`);
                throw new NotFoundException(`The user with id: ${user} does not exist in the db`);
            }

            
        } catch (error) {
            this.logger.error(`Error adding contacts: ${error}`);
            if(error instanceof NotFoundException || error instanceof BadRequestException)
                throw error;
            else
                throw new InternalServerErrorException(`Error adding contacts: ${error}`);
        }
    }

    //Validation methods
    async validateContacts(contacts: Array<NewContactDTO>, currentContacts?: Array<NewContactDTO>):Promise<AddContactsResponse>{
        try {

            let noValid = [];
            let filteredContacts = [];
            let validContacts = [];

            if(currentContacts){
                this.logger.debug('The user has contacts yet');

                const newContacts = currentContacts.filter(c => {
                    if(contacts.some(current => current.phone === c.phone)){
                        this.logger.debug('Contact registered'); 
                        noValid = [...noValid, {message: 'Contact already registered', ...c}];
                    }else{ 
                        this.logger.debug('New contact');
                        return c;
                    }
                });

                filteredContacts = [...filteredContacts, ...newContacts];

                validContacts = await this.validateContactsPhone(filteredContacts);
            }else{
                validContacts = await this.validateContactsPhone(contacts);
            }

            if(validContacts){

                const badPhoneContacts = contacts.filter(({phone: phone1}) => validContacts.some(({phone: phone2}) => phone1 !== phone2));

                if(badPhoneContacts){
                    badPhoneContacts.forEach(c => noValid = [...noValid, {message:'Invalid phone number', ...c}]);

                    return {savedContacts: validContacts, invalidContacts: noValid};
                }else if(noValid.length > 0){
                    return {savedContacts: validContacts, invalidContacts: noValid};
                }
            }else
                return {invalidContacts: noValid}
                
        } catch (error) {
            this.logger.error(`Error validating contacts: ${error}`);
            throw new InternalServerErrorException('Error validating contacts');
        }
    }

    //look user by phone
    async getUserByPhone(phone:string):Promise<User>{
        try {
            this.logger.debug(`getting user by phone ${phone}`);
            const user = await this.usersModel.findOne({phone});
            return user;
        } catch (error) {
            this.logger.error(`Error getting user by phone: ${error}`);
            throw new InternalServerErrorException('Error getting user by phone');
        }
    }

    //look user
    async getUser(id:string):Promise<User>{
        try {
            this.logger.debug('Looking user by id');
            const user = await this.usersModel.findById(id).exec();
            return user;
        } catch (error) {
            this.logger.error(`Error looking user by id: ${error}`);
            throw new InternalServerErrorException('Error looking user by id');
        }
    }

    async validateUsersToCompare(user1: User, user2: User):Promise<boolean>{
        try {
            if(!user1 && user2)
                throw new NotFoundException(`Users ${user1.id} and ${user2.id} does not exist in database`);
            else if(!user1)
                throw new NotFoundException(`User ${user1.id} does not exist in database`);
            else if(!user2)
                throw new NotFoundException(`User ${user2.id} does not exist in database`);

            if(!user1.contacts && !user2.contacts)
                throw new BadRequestException('Users must have registered contacts');
            else if(!user1.contacts)
                throw new BadRequestException(`User ${user1.id} does not have contacts registered`);
            else if(!user2.contacts)
                throw new BadRequestException(`User ${user2.id} does not have contacts registered`);


            return true;
        } catch (error) {
            this.logger.debug('Error validating users');

            if(error instanceof NotFoundException || error instanceof BadRequestException)
                throw error;
            else
                throw new InternalServerErrorException('Error validating users to compare');       
        }
    }

    async getRegisteredContacts(user: User):Promise<Array<User>>{
        try {
            this.logger.debug('Getting registered users');
            const userPromises = await user.contacts.map(async c => {
                const user = await this.getUserByPhone(c.phone);
                if(user) return user;
            });
            return Promise.all(userPromises);
        } catch (e) {
            this.logger.error('Error getting users from db');
            throw new InternalServerErrorException('Error getting users from db');
        }
    }

    async validateContactsPhone(contacts: Array<NewContactDTO>):Promise<Array<NewContactDTO>>{
        try {
            if(contacts.length > 0){
                const validContacts = contacts.filter(async c => {
                    const {valid} = await this.phoneValidatorService.validatePhone(c.phone);
                    if(valid) return c;
                });
                
                return Promise.all(validContacts);
            }else{
                return null;
            }
        } catch (e) {
            this.logger.error(`Error validating contacts phone ${e}`);
            throw new InternalServerErrorException('Error validating contacts phone');
        }
    }
}