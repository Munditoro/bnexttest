import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersSchema } from '../users/model/users.schema';
import { PhoneModule } from 'src/phone/phone.module';

@Module({
  imports:[
    MongooseModule.forFeature([{name: 'Users', schema: UsersSchema}]),
    PhoneModule
  ],
  providers: [UsersService],
  controllers: [UsersController]
})
export class UsersModule {}
