import { Body, Controller, Get, HttpStatus, NotFoundException, Param, Post, Res } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AddContactsDTO } from './model/dtos/addcontacts.dto';
import { CommonDTO } from './model/dtos/common.dto';
import { NewUserDTO } from './model/dtos/newuser.dto';
import { UrlValidator } from './model/dtos/ulr.validator.dto';
import { UsersService } from './users.service';

@ApiTags('Users')
@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService){}

    @ApiResponse({status:HttpStatus.CREATED})
    @Post('create')
    async createUser(@Body() newUserDTO:NewUserDTO, @Res() res){
        const newUser = await this.usersService.createUser(newUserDTO);

        return res.status(HttpStatus.CREATED).json({
            message: 'User created',
            newUser
        });
    }

    @ApiResponse({status:HttpStatus.CREATED})
    @Post('addContacts')
    async addContacts(@Body() addContactsDTO:AddContactsDTO, @Res() res){
        const contacts = await this.usersService.addContacts(addContactsDTO);

        return res.status(HttpStatus.CREATED).json({
            message: `Contacts added to user: ${addContactsDTO.user}`,
            contacts
        });
    }

    @ApiResponse({status:HttpStatus.OK})
    @Post('compare')
    async compareContacts(@Body() commonDTO:CommonDTO, @Res() res){
        const result = await this.usersService.compareContacts(commonDTO);

        return res.status(HttpStatus.OK).json({
            message: 'Compare sucessful',
            result
        });
    }

    @ApiResponse({status:HttpStatus.OK})
    @Get('contacts/:id')
    async getUser(@Param() params:UrlValidator, @Res() res){
        const contacts = await this.usersService.getUserContacts(params);

        if(!contacts) throw new NotFoundException(`User ${params.id} has not contacts yet`);

        return res.status(HttpStatus.OK).json({
            message: 'Contacts found',
            contacts
        });
    }
}
