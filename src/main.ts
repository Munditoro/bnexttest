import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { UsersModule } from './users/users.module';

async function bootstrap() {

  const app = await NestFactory.create(AppModule);
  app.enableCors();

  app.setGlobalPrefix(`api/V1`);
  app.useGlobalPipes(new ValidationPipe());


  const options = new DocumentBuilder()
                  .setTitle('Bnext API')
                  .setDescription('Bnext Backend Test API')
                  .setVersion('V1')
                  .addTag('Bnext')
                  .build();

  const appDocument = SwaggerModule.createDocument(app, options, {
    include:[
      UsersModule
    ]
  });

  SwaggerModule.setup('api', app, appDocument);

  await app.listen(process.env.PORT || 3000);
}
bootstrap();